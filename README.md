# Aws ECS cloudwatch dashboard

## Usage

```bash
module "aws_ecs_cloudwatch_dashboard" {
  source                   = "git::https://gitlab.com/keltiotechnology/terraform/modules/aws/aws-ecs-cloudwatch-dashboard"

  ecs_cluster_name         = module.ecs_cluster.ecs_cluster_name
  ecs_tasks_templates      = [
    "keltio-dev-frontend_template",
    "keltio-prod-backend_template"
  ]
  ecs_region               = var.region
}
```

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 3.56.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_cloudwatch_dashboard.ecs](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_dashboard) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_dashboard_name"></a> [dashboard\_name](#input\_dashboard\_name) | Name of the ECS cloudwatch dashboard | `string` | `"ECS"` | no |
| <a name="input_ecs_cluster_name"></a> [ecs\_cluster\_name](#input\_ecs\_cluster\_name) | Name of the ECS cluster present in dashboard | `string` | n/a | yes |
| <a name="input_ecs_region"></a> [ecs\_region](#input\_ecs\_region) | ECS region | `string` | n/a | yes |
| <a name="input_ecs_tasks_templates"></a> [ecs\_tasks\_templates](#input\_ecs\_tasks\_templates) | ECS tasks templates that will be present in dashboard | `list(string)` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_dashboard_body"></a> [dashboard\_body](#output\_dashboard\_body) | n/a |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
