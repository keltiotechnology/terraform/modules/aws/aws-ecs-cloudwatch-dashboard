locals {
  dashboard_body = templatefile("${path.module}/templates/ecs_dashboard.tpl", {
    ecs_cluster_name    = var.ecs_cluster_name
    ecs_region          = var.ecs_region
    ecs_tasks_templates = var.ecs_tasks_templates
    }
  )
}

resource "aws_cloudwatch_dashboard" "ecs" {
  dashboard_name = var.dashboard_name
  dashboard_body = local.dashboard_body
}
