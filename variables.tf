variable "dashboard_name" {
  type        = string
  description = "Name of the ECS cloudwatch dashboard"
  default     = "ECS"
}

variable "ecs_cluster_name" {
  type        = string
  description = "Name of the ECS cluster present in dashboard"
}

variable "ecs_region" {
  type        = string
  description = "ECS region"
}

variable "ecs_tasks_templates" {
  type        = list(string)
  description = "ECS tasks templates that will be present in dashboard"
}
